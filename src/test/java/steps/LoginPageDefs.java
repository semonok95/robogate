package steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import pages.LoginPage;

import static com.codeborne.selenide.Selenide.open;

public class LoginPageDefs {

    private LoginPage loginPage = new LoginPage();

    @Given("Open login page")
    public void openLoginPage() {
        open(loginPage.getUrl());
    }

    @When("Enter email {string}")
    public void enterEmail(String email) {
        loginPage.getEmailField().setValue(email);

    }

    @And("Enter password {string}")
    public void enterPassword(String password) {
        loginPage.getPasswordField().setValue(password);
    }

    @And("Click on {string}")
    public void clickOn(String login) {
        loginPage.getElementByText(login).click();
    }

    @Then("You receive message {string}")
    public void youReceiveMessage(String ExpectedError) {
        String ActualError = loginPage.getMainError().getText();
        Assert.assertEquals(ExpectedError, ActualError);
    }
}
