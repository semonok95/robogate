Feature: Login tests

  Scenario: Wrong login/password
    Given Open login page
    When Enter email "semonok95@gmail.com"
    And Enter password "Password123"
    And Click on "Login"
    Then You receive message "Wrong login/password"