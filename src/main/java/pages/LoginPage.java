package pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import lombok.Getter;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class LoginPage extends BasePage {

    @Getter
    private SelenideElement emailField = $(byXpath("//input[@class='input__field input__field_mode_email']"));

    @Getter
    private SelenideElement passwordField = $(byXpath("//input[@class='password__field password__field_mode_password']"));

    @Getter
    private SelenideElement mainError =
            $(byXpath("//div[@class='tooltip tooltip_pos_top form__tooltip tooltip_show_true']/div/div[5]"));


    @Override
    public String getUrl() {
        return "https://webtrader.roboforex.com/";
    }


}
