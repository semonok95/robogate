package pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

public abstract class BasePage {

    protected abstract String getUrl();

    public SelenideElement getElementByText(String name){
        return $(byText(name));
    }

}
